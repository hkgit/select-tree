const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = merge(common, {
    devtool: 'inline-source-map',
    devServer: {
        contentBase: path.join(__dirname, "../dist"),
        hot: true,
        inline: true,
        compress: true, // 启用gzip压缩
        host: 'localhost',
        port: 8090,
        open: true,
        openPage: '/select-tree.html'
    },
    output: {
        filename: '[name].js'
    },
    module: {
        rules: [{
            test: /\.css$/,
            // css-hot-loader 无刷新加载css文件变化
            use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: 'css-loader'
            }))
        }]
    },
    plugins: [
        // 自动生成html
        new HtmlWebpackPlugin({
            template: './src/select-tree.html',
            filename: 'select-tree.html'
        }),
        // 单独打包css
        new ExtractTextPlugin({
            filename: (getPath) => {
                return getPath('[name]') == 'vendor' ? getPath('vendor/[name].css') : getPath('[name].css');
            }
        }),
        // 单独打包jq及jq插件，此插件的依赖库单独抽出来，不影响插件的开发
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: "vendor",
        //     filename: "vendor/[name].js"
        // }),
        // 开启热启动
        new webpack.HotModuleReplacementPlugin()
    ]
});