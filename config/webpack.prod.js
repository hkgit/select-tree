// const webpack = require('webpack');
const merge = require('webpack-merge');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    // devtool: 'source-map',
    module: {
        rules: [{
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: {
                    loader: 'css-loader',
                    options: {
                        minimize: true, // 压缩css
                        sourceMap: true
                    }
                }
            })
        }]
    },
    output: {
        filename: '[name].min.js'
    },
    plugins: [
     	// 自动生成html
    	new HtmlWebpackPlugin({
            template: './src/select-tree.html',
            filename: 'select-tree.html',
            hash: true,
            minify: { // 压缩HTML文件
                removeComments: true, // 移除HTML中的注释
                collapseWhitespace: true // 删除空白符与换行符
            }
        }),
        // 单独打包css
        new ExtractTextPlugin({
            filename: (getPath) => {
                return getPath('[name]') == 'vendor' ? getPath('vendor/[name].css') : getPath('[name].css');
            }
        }),
        // 单独打包jq及jq插件，此插件的依赖库单独抽出来，不影响插件的开发
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: "vendor",
        //     filename: "vendor/[name].min.js"
        // }),
        // 压缩js
        new UglifyJSPlugin({
            sourceMap: true
        })
    ]
});