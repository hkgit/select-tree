const path = require('path');
// const webpack = require('webpack');

module.exports = {
    entry: {
        // 把需要引入的插件单独分出一个入口，和插件主体分开
        // 'vendor': [
        //     'jquery',
        //     'ztree',
        //     'bootstrap/dist/css/bootstrap.css',
        //     'ztree/css/metroStyle/metroStyle.css'
        // ],
        // 插件主体js
        'select-tree': './src/select-tree.js'
    },
    output: {
        path: path.resolve(__dirname, '../dist'),
        library: 'selectTree', // 插件名称
        libraryTarget: 'umd' // 插件支持CommonJS2，CommonJS，amd，var
    },
    // module: {
    //     rules: [{
    //         test: /\.(png|svg|jpg|gif|woff|woff2|eot|ttf|otf)$/,
    //         use: [{
    //             loader: 'file-loader',
    //             options: {
    //                 publicPath: '../',
    //                 outputPath: 'vendor/'
    //             }
    //         }]
    //     }]
    // },
    // plugins: [
    //     自动加载jq
    //     new webpack.ProvidePlugin({
    //         $: 'jquery',
    //         jQuery: 'jquery'
    //     })
    // ]
}